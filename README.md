# CoralModel

CoralModel is a JAVA project that contains CORAL models for [VEGA](https://www.vegahub.eu/portfolio-item/vega-qsar/) platform. The implemented models are based on Monte Carlo technique and have been built using [CORAL](http://www.insilico.eu/coral/SOFTWARECORAL.html) software. 

## Installation

### Installing dependencies

CoralModel project is developed in JAVA using Apache Netbeans 12.5 as IDE. It uses the following dependences:

* Open JDK 11

* cdk 2.3

* Core-1.3.8-jar-with-dependencies.jar

## Reference

The models were implemented within the JANUS (FKZ 3716 65 4140), LIFE COMBASE [LIFE15 ENV/ES/000416], VERMEER (LIFE16 ENV/IT/000167), CONCERT REACH (LIFE17 GIE/IT/000461) and EU-ToxRisk projects.

Further details about the implemented models can be found in the following publications:

* Selvestrel, G.; Lavado, G.J.; Toropova, A.P.; Toropov, A.A.; Gadaleta, D.; Marzo, M.; Baderna, D.; Benfenati, E. Monte Carlo Models for Sub-Chronic Repeated-Dose Toxicity: Systemic and Organ-Specific Toxicity. Int. J. Mol. Sci. 2022, 23, 6615. https://doi.org/10.3390/ijms23126615 

* Toma, C., Gadaleta, D., Roncaglioni, A. et al. QSAR Development for Plasma Protein Binding: Influence of the Ionization State. Pharm Res 36, 28 (2019). https://doi.org/10.1007/s11095-018-2561-8

* Andrey A. Toropov, Alla P. Toropova, Giuseppa Raitano, Emilio Benfenati.
CORAL: Building up QSAR models for the chromosome aberration test, Saudi Journal of Biological Sciences, Volume 26, Issue 6, 2019, Pages 1101-1106, ISSN 1319-562X. https://doi.org/10.1016/j.sjbs.2018.05.013.

* Alla P. Toropova, Andrey A. Toropov. CORAL: QSAR models for carcinogenicity of organic compounds for male and female rats, Computational Biology and Chemistry,
Volume 72, 2018, Pages 26-32, ISSN 1476-9271. 
https://doi.org/10.1016/j.compbiolchem.2017.12.012.

* Alla P. Toropova, Andrey A. Toropov. CORAL: Binary classifications (active/inactive) for drug-induced liver injury, Toxicology Letters, Volume 268, 2017, Pages 51-57, ISSN 0378-4274. https://doi.org/10.1016/j.toxlet.2017.01.011.

* Toropov, A.A., Toropova, A.P., Pizzo, F., Lombardo A, Gadaleta D. and Benfenati E. CORAL: model for no observed adverse effect level (NOAEL). Mol Divers 19, 563–575 (2015). https://doi.org/10.1007/s11030-015-9587-1

* A.A. Toropov, A.P. Toropova, A. Lombardo, A. Roncaglioni, G.J. Lavado, and E. Benfenati. The monte carlo method to build up models of the hydrolysis half-lives of organic compounds. SAR and QSAR in Environmental Research, 0(0):1–9, 2021. https://doi.org/10.1080/1062936X.2021.1914156

* Marco Marzo, Giovanna J. Lavado, Francesca Como, Andrey A. Toropov, Alla P. Toropova, Diego Baderna, Claudia Cappelli, Anna Lombardo, Cosimo Toma, Maria Blázquez Sánchez, and Emilio Benfenati. QSAR models for biocides: The example of the prediction of daphnia magna acute toxicity. SAR and QSAR in Environmental Research, 31(3):227–243, 2020 Mar. https://doi.org/10.1080/1062936X.2019.1709221

* Lavado, Giovanna J., Domenico Gadaleta, Cosimo Toma, Azadi Golbamaki, Andrey A. Toropov, Alla P. Toropova, Marco Marzo, Diego Baderna, Jürgen Arning, and Emilio Benfenati. Zebrafish AC50 modelling: (Q)SAR models to predict developmental toxicity in zebrafish embryo. Ecotoxicology and Environmental Safety, 202:110936, oct 2020. https://doi.org/10.1016/j.ecoenv.2020.110936

## Contributing and credits

CoralModel project was originally created by Giovanna Janet Lavado at [Laboratory of Environmental Chemistry and Toxicology](https://www.marionegri.it/laboratori/laboratorio-chimica-e-tossicologia-dellambiente), Department of Environmental Health Sciences, Istituto di Ricerche Farmacologiche Mario Negri - IRCCS.

## License
CoralModel is licensed under [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)