
 /**
 * CORAL model for 90 compounds with experimental data for general LOAEL in Rats for oral sub-chronic RDT. 
 * The experimental data is expressed in mg/Kg bw (as log units)
 * The optimal descriptor is based on attributes extracted from SMILES (Sk, SSk,).
 */
package insilico.core.coral.models.loael.brain;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author MMarzo
 */
public class CoralBrainLoael extends CoralModel{

    //Endpoint =   2.5305357 (± 0.0383210) +    0.1089317 (± 0.0053590) * DCW(1,4)
    public CoralBrainLoael() {
        super(2.5305357, 0.1089317, "/insilico/core/coral/models/loael/brain/attr_cw.txt");
    }
    
        @Override
    protected double calculateDCW(String smi) throws Exception {
        double dcw = 0;
        
        CoralSMILES coral_smi = new CoralSMILES();
        ArrayList<String> list_tokens = coral_smi.ProcessSMILES(smi);
        
        
        //Sk, SSk, 
        ArrayList<String> list_attributes = CoralAttributes.calculateAttributeSks(list_tokens);
         list_attributes.addAll(CoralAttributes.calculateAttributeSSks(list_tokens));
      
         ArrayList<Double> weights = calculateSAvalues(list_attributes);
         
        // ADDITIVE scheme for the optimal descriptor
        for (Double d : weights){
            dcw += d;
        }
        return dcw;
    }
}
    

