/**
 * CORAL model for 558 compounds with experimental data for general LOAEL in Rats for oral sub-chronic RDT. 
 * The experimental data is expressed in mg/kg bw (as logarithmic units)
 * The optimal descriptor is based on attributes extracted from SMILES (Sk, SSk, SSSk, APP).
 */
  
package insilico.core.coral.models.loael.general;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 */
public class CoralGeneralLoael extends CoralModel{

    // Endpoint =   2.6738599 (± 0.0057358) +    0.1967440 (± 0.0009514) * DCW(1,30)
    public CoralGeneralLoael(){
        super(2.6738599,0.1967440, "/insilico/core/coral/models/loael/general/attr-cw.txt");
    }
    @Override
    protected double calculateDCW(String smi) throws Exception {
        double DCW = 0;
        
        // tokens from SMILES string
        CoralSMILES coral_smi = new CoralSMILES();        
        ArrayList<String> token_list = coral_smi.ProcessSMILES(smi);
        
        // Sk,SSk,SSSk
        ArrayList<String> attr_list = CoralAttributes.calculateAllLocalAttributes(token_list);
        
        // APP: Cl and N, O, S, P
        ArrayList<String> chemicalList = new ArrayList<>();
        chemicalList.add("N");
        chemicalList.add("O");
        chemicalList.add("S");
        chemicalList.add("P");
            
        ArrayList<String> attrAPPList = 
                CoralAttributes.calculateAttributesVectorAPP(token_list, "Cl", chemicalList);

        attr_list.addAll(attrAPPList);
        
        ArrayList<Double> weights = calculateSAvalues(attr_list);
        for (Double cw : weights) {
            DCW += cw;
        }
        
        return DCW;
    }
    
}
