/**
 * CORAL model for 283 compounds with experimental data for general LOAEL in Rats for oral sub-chronic RDT in kidney. 
 * The experimental data is expressed in mg/kg bw (as logarithmic units)
 * The optimal descriptor is based on attributes extracted from SMILES (Sk, SSk, SSSk).
 */

package insilico.core.coral.models.loael.kidney;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author Domenico Gadaleta
 */
public class CoralKidneyLoael extends CoralModel{

    // Endpoint =   2.4150252 (± 0.0053276) +    0.1027443 (± 0.0010802) * DCW(1,10)
    public CoralKidneyLoael(){
        super(2.4150252,0.1027443, "/insilico/core/coral/models/loael/kidney/attr-cw.txt");
    }
    @Override
    protected double calculateDCW(String smi) throws Exception {
        double DCW = 0;
        
        // tokens from SMILES string
        CoralSMILES coral_smi = new CoralSMILES();        
        ArrayList<String> token_list = coral_smi.ProcessSMILES(smi);
        
        // Sk,SSk,SSSk
        ArrayList<String> attr_list = CoralAttributes.calculateAllLocalAttributes(token_list);
        
        ArrayList<Double> weights = calculateSAvalues(attr_list);
        for (Double cw : weights) {
            DCW += cw;
        }
        
        return DCW;
    }            
    
}
