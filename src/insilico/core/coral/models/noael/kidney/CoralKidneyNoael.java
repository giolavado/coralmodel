/**
 * CORAL model for 280 compounds with experimental data for kidney NOAEL in Rats for oral sub-chronic RDT. 
 * The experimental data is expressed in mg/kg bw (as logarithmic units)
 * The optimal descriptor is based on attributes extracted from SMILES (Sk,SSk,SSSk, APP).
 */
package insilico.core.coral.models.noael.kidney;


import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 */
public class CoralKidneyNoael extends CoralModel{

    //Endpoint =   2.1060212 (± 0.0059588) +    0.1735682 (± 0.0016214) * DCW(1,10)

    public CoralKidneyNoael(){
        super(2.1060212, 0.1735682 , "/insilico/core/coral/models/noael/kidney/attr-cw.txt");
    }
            
    @Override
    protected double calculateDCW(String smi) throws Exception {
        double dcw = 0;
        
        CoralSMILES coral_smi = new CoralSMILES(); 
        ArrayList<String> list_tokens = coral_smi.ProcessSMILES(smi);
        
        // Sk,SSk,SSSk
        ArrayList<String> list_attributes = CoralAttributes.calculateAllLocalAttributes(list_tokens);
        
       // APP: Cl and N,O
        ArrayList<String> chemicalList = new ArrayList<>();
        chemicalList.add("N");
        chemicalList.add("O");
        
        ArrayList<String> attrAPPList = 
                CoralAttributes.calculateAttributesVectorAPP(list_tokens, "Cl", chemicalList);

        list_attributes.addAll(attrAPPList);
        
        ArrayList<Double> weights = calculateSAvalues(list_attributes);
        
        // ADDITIVE scheme for the optimal descriptor
        for (Double d : weights){
            dcw += d;
        }
        
        return dcw;
    }


 
    
}
