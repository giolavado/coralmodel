/**
 * A Coral model for 326 compounds with NOAEL subchronic toxicity data was implemented under the project CONCERT REACH.
 * The experimental data is expressed in mg/kgbw (as logarithmic units). 
 * The model involves Sk, SSk and HARD attributes.
 */
package insilico.core.coral.models.noael;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 */
public class CoralNOAEL extends CoralModel{

    public CoralNOAEL() {
        //intercept, slope and list of att-cw
        super(1.1574991, 0.0608755 , "/insilico/core/coral/models/noael/noael_attr_cw.txt");
    }

    @Override
    protected double calculateDCW(String smi) throws Exception {
        
        // calculate the list of all Sk, SSk and HARD attributes for the given SMILES
        CoralSMILES coral_smi = new CoralSMILES();
        ArrayList<String> tokenList =  coral_smi.ProcessSMILES(smi);
        
        ArrayList<String> attrList = CoralAttributes.calculateAttributeSks(tokenList);
        attrList.addAll(CoralAttributes.calculateAttributeSSks(tokenList));
        attrList.add(CoralAttributes.calculateAttributeHARDCoral2017(tokenList));

        // retrieve the weights of all attributes for the current loaded model
        ArrayList<Double> weights = calculateSAvalues(attrList);
        
        // calculate the prediction with an ADDITIVE scheme
        double value = 0;
        for (Double d : weights)
            value += d;
        
        return value;
    }
    
}
