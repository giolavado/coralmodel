/**
 * CORAL model for 90 compounds with experimental data for general NOAEL in Rats for oral sub-chronic RDT. 
 * The experimental data is expressed in mg/kg bw (as logarithmic units)
 * The optimal descriptor is based on attributes extracted from SMILES (Sk, APP).
 */
package insilico.core.coral.models.noael.brain;


import insilico.core.coral.CoralAttributes;

import insilico.core.coral.CoralModel;
import insilico.core.coral.CoralSMILES;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 */
public class CoralBrainNoael extends CoralModel{

    // Endpoint =   2.3962455 (± 0.0337524) +    0.2029147 (± 0.0106681) * DCW(1,33)
    public CoralBrainNoael(){
        super(2.3962455, 0.2029147, "/insilico/core/coral/models/noael/brain/attr-cw.txt");
    }
            
    @Override
    protected double calculateDCW(String smi) throws Exception {
        double dcw = 0;
        
        CoralSMILES coral_smi = new CoralSMILES(); 
        ArrayList<String> list_tokens = coral_smi.ProcessSMILES(smi);
        
        // Sk
        ArrayList<String> list_attributes = CoralAttributes.calculateAttributeSks(list_tokens);
        
       // APP: All
        ArrayList<String> attrAPPList = 
                CoralAttributes.calculateAttributesAllVectorAPP(list_tokens);

        list_attributes.addAll(attrAPPList);
        
        ArrayList<Double> weights = calculateSAvalues(list_attributes);
        
        // ADDITIVE scheme for the optimal descriptor
        for (Double d : weights){
            dcw += d;
        }
        
        return dcw;
    }


 
    
}
