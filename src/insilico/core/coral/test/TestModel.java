package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.PPB.CoralPPB;
import insilico.core.coral.models.airhalflife.CoralAirHalfLife;
import insilico.core.coral.models.biocides.daphniamagna.OptimizedCoralDaphnia;
import insilico.core.coral.models.biocides.fish.OptimizedCoralFish;
import insilico.core.coral.models.carcino.CoralFRCarcinogenicity;
import insilico.core.coral.models.carcino.CoralMRCarcinogenicity;
import insilico.core.coral.models.dili.CoralDILI;
import insilico.core.coral.models.hydrolysis.CoralHydrolysis;
import insilico.core.coral.models.loael.CoralLOAEL;
import insilico.core.coral.models.loael.brain.CoralBrainLoael;
import insilico.core.coral.models.loael.general.CoralGeneralLoael;
import insilico.core.coral.models.loael.kidney.CoralKidneyLoael;
import insilico.core.coral.models.loael.liver.CoralLiverLoael;
import insilico.core.coral.models.noael.CoralNOAEL;
import insilico.core.coral.models.noael.GAOnoael;
import insilico.core.coral.models.noael.brain.CoralBrainNoael;
import insilico.core.coral.models.noael.general.CoralGeneralNoael;
import insilico.core.coral.models.noael.kidney.CoralKidneyNoael;
import insilico.core.coral.models.noael.liver.CoralLiverNoael;
import insilico.core.coral.models.skin.permeation.CoralSkinPermeation;
import insilico.core.coral.models.skin.sensitization.CoralSkinSensitization;
import insilico.core.coral.models.zebrafish.CoralZebraFish;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Giovanna Lavado
 */
public class TestModel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        
        // check there are 3 args
        if (args.length!=3) throw new IllegalArgumentException("Exactly 3 parameters required !");
        
        String smi_file = args[1];
        String preds_file = args[2]; 
        
        // Verify first arg is an integer
        try {
            int id_model = Integer.parseInt(args[0]);
          
            /* List of models: id_model, model
              id_model=1 zebrafish embryo 
              id_model=2 daphnia magna 
              id_model=3 fish 
              4 half-life air
              5 hydrolysis 
              6 NOAEL (GAO)
              7 NOAEL (Sk, SSk and HARD attributes)
              8 LOAEL (Sk, SSk and HARD attributes)
              9 Hepatotoxicity DILI
              10 Carcinogenicity (female)
              11 Carcinogenicity (male)
              12 Skin sensitization
              13 Skin permeation
              14 PPB
              15 NOAEL general
              16 LOAEL general
              17 NOAEL liver
              18 LOAEL liver
              19 NOAEL brain
              20 LOAEL brain
              21 NOAEL kidney
              22 LOAEL kidney

              */

            CoralModel model = null;
            switch (id_model){
                case 1:
                    model = new CoralZebraFish();
                    break;
                case 2:
                    model = new OptimizedCoralDaphnia();
                    break;
                case 3:
                    model = new OptimizedCoralFish();
                    break;
                case 4:
                    model = new CoralAirHalfLife();
                    break;
                case 5:
                    model = new CoralHydrolysis();
                    break;
                case 6:
                    model = new GAOnoael();
                    break;
                case 7:
                    model = new CoralNOAEL();
                    break;
                case 8:
                    model = new CoralLOAEL();    
                    break;
                case 9:
                    model = new CoralDILI();
                    break;
                case 10:
                    model = new CoralFRCarcinogenicity();
                    break;
                case 11:
                    model = new CoralMRCarcinogenicity();
                    break;
                case 12:
                    model = new CoralSkinSensitization();
                    break;
                case 13:
                    model = new CoralSkinPermeation();
                    break;
                case 14:
                    model = new CoralPPB();
                    break;
                case 15:
                    model = new CoralGeneralNoael();
                    break;
                case 16:
                    model = new CoralGeneralLoael();
                    break;
                case 17:
                    model = new CoralLiverNoael();
                    break;
                case 18:
                    model = new CoralLiverLoael();
                    break;            
                case 19: // 19 NOAEL brain
                    model = new CoralBrainNoael();
                    break;               
                case 20:
                    model = new CoralBrainLoael();
                    break;       
                case 21:
                    model = new CoralKidneyNoael();
                    break;    
                case 22:
                    model = new CoralKidneyLoael ();
                    break;
            }

            if (model!=null){
                BufferedReader reader = new BufferedReader(new FileReader(smi_file));
                BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));

                String line;

                while ((line = reader.readLine()) != null){
                    double pred = model.Predict(line.trim());
                    writer.write(Double.toString(pred));
                    writer.write("\r\n");
                    System.out.println(pred);
                }

                reader.close();
                writer.close(); 
            }

        } catch (NumberFormatException nbfe) {
          
          throw new IllegalArgumentException("First arg must be an integer");

        }
        

        
        
    }
}
