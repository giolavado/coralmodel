package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.dili.CoralDILI;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class testDILI {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
              
        CoralModel model = new CoralDILI();

 /*
CCCl                                                                                                                                                                                                                                                                                                                                      
CN1CCN(CCCN2C3=C(SC4=C2C=CC=C4)C=CC=C3)CC1                                                                                                                                                                                                                                                                                                
CCC1(CCC(=O)NC1=O)C1=CC=CC=C1                                                                                                                                                                                                                                                                                                             
[H][C@@]1(O[C@@H]2[C@@H](O)[C@H](N)C[C@H](N)[C@@]2([H])O[C@H]2O[C@H](CN)[C@@H](O)[C@H](O)[C@H]2N)O[C@H](CO)[C@@H](O[C@@]2([H])O[C@@H](CN)[C@@H](O)[C@H](O)[C@H]2N)[C@H]1O                                                                                                                                                                 
CCCCCCC1=C(O)C=C(O)C=C1                                                                                                                                                                                                                                                                                                                   
NC(N)=O                                                                                                                                                                                                                                                                                                                                   
[H][C@]12CN(C)C[C@]1([H])C1=CC(Cl)=CC=C1OC1=CC=CC=C21                                                                                                                                                                                                                                                                                     
CN1C(CC(O)C2=CC=CC=C2)CCCC1CC(=O)C1=CC=CC=C1                                                                                                                                                                                                                                                                                              
CC12CCC3C(CCC45OC4C(O)=C(CC35C)C#N)C1CCC2O                                                                                                                                                                                                                                                                                                
[H][C@@]12CC[C@](C)(O)[C@@]1(C)CC[C@@]1([H])[C@@]2([H])[C@@H](C)CC2=CC(=O)CC[C@]12C                                                                                                                                                                                                                                                       
  */       
        
        //String smi = "CCCl"; //ok
//        String smi = "CN1CCN(CCCN2C3=C(SC4=C2C=CC=C4)C=CC=C3)CC1"; //ok
        //String smi = "CCC1(CCC(=O)NC1=O)C1=CC=CC=C1";
        //String smi = "[H][C@@]1(O[C@@H]2[C@@H](O)[C@H](N)C[C@H](N)[C@@]2([H])O[C@H]2O[C@H](CN)[C@@H](O)[C@H](O)[C@H]2N)O[C@H](CO)[C@@H](O[C@@]2([H])O[C@@H](CN)[C@@H](O)[C@H](O)[C@H]2N)[C@H]1O"; // 
        //
//        String smi = "CCCCCCC1=C(O)C=C(O)C=C1";
        
/*        
        
       // String smi = "[H][C@]12CN(C)C[C@]1([H])C1=CC(Cl)=CC=C1OC1=CC=CC=C21"; // diff
        double pred = model.Predict(smi);
        System.out.println(pred);
*/        


        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = model.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();



    }

}
