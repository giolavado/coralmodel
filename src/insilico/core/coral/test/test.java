package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.zebrafish.CoralZebraFish;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Alberto
 */
public class test {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        CoralModel model = new CoralZebraFish();

        String smi_file = args[0];
        
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = model.Predict(line.trim());
            System.out.println(pred);
        }
        reader.close();

    }    
}
