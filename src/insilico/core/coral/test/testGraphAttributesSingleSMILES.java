package insilico.core.coral.test;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralGraphAttributes;
import insilico.core.coral.CoralSMILES;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import java.util.ArrayList;
import org.openscience.cdk.exception.CDKException;

/**
 *
 * @author Giovanna Lavado
 */
public class testGraphAttributesSingleSMILES {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws GenericFailureException, InvalidMoleculeException, CDKException, Exception {
        
        
        
        ArrayList<String> SMILESList = new ArrayList<>();
        
       
        //SMILESList.add("O=C1NN=C(C(F)(F)F)S1");
        //SMILESList.add("c1ccccc1");
        //SMILESList.add("Clc1ccccc1");
        //SMILESList.add("O=C(/C=C/c1ccccc1)c2ccc(O)cc2");
        //SMILESList.add("C1=CC=C(C=C1)C2=CC=CC=C2[O-].[Na+]");
        
        //SMILESList.add("Fc1ccc(cc1)[Si](c2ccc(F)cc2)(C)Cn3ncnc3");
        //SMILESList.add("Clc1ccccc1C(c2ccccc2)(c3ccccc3)n4ccnc4");
        
        //SMILESList.add("N#Cc1c(c(C#N)c(c(c1Cl)Cl)Cl)Cl");
        
        
        //SMILESList.add("O=C(N4CCC(c1nc(cs1)C3=NOC(c2c(F)cccc2(F))C3)CC4)Cn5nc(cc5C)C(F)(F)F");
        
        //SMILESList.add("O=C(Nc1onc(c1)C(C)(CC)CC)c2c(OC)cccc2(OC)");
        
//        SMILESList.add("O=[N+]([O-])N=C1N(C)COCN1Cc2cnc(Cl)s2");

        
        //SMILESList.add("O=C(Nc1onc(c1)C(C)(CC)CC)c2c(OC)cccc2(OC)");      
        //SMILESList.add("C1=CC(=NC(=C1)Cl)C(Cl)(Cl)Cl"); 
        
        //SMILESList.add("n1c[nH]cc1C(c2cccc(c2C)C)C"); 
                
        
       //SMILESList.add("O=C6OC(CC)CCCC(OC1OC(C)C(N(C)C)CC1)C(C(=O)C5=CC3C(C=C(C)C4CC(OC2OC(C)C(OC)C(OC)C2(OC))CC34)C5C6)C");
        
       // SMILESList.add("O=C(OC)N(C(=O)N2N=C3c1ccc(cc1CC3(OC2)(C(=O)OC))Cl)c4ccc(OC(F)(F)F)cc4");

       //SMILESList.add("N#Cc2c(c1ccc(cc1)Cl)n(c(c2Br)C(F)(F)F)COCC");
        
                
        // heteroatoms
    /*    SMILESList.add("Fc1ccc(cc1)[Si](c2ccc(F)cc2)(C)Cn3ncnc3");
        SMILESList.add("O=[N+]([O-])N=C1N(C)COCN1Cc2cnc(Cl)s2");
        SMILESList.add("N#Cc1nn(c(N)c1S(=O)C(F)(F)F)c2c(cc(cc2Cl)C(F)(F)F)Cl");
        
    */            

        //SMILESList.add("C13(C4(C2(C5(C(C1(C2(Cl)Cl)Cl)(C3(C(C45Cl)(Cl)Cl)Cl)Cl)Cl)Cl)Cl)Cl");
        
        // for NOAEL
        //SMILESList.add("O=C1NSc2ccccc12"); // java: $10011100000 // coral: $10011000000: CW:3.2498
        //SMILESList.add("O=C(OCC)CSc1nc(nn1(C(=O)N(C)C))C(C)(C)C"); // java: $10011100000 //coral  $10011000000: CW:3.2498
        //SMILESList.add("C1(C(C(C(C(C1Cl)Cl)Cl)Cl)Cl)Cl");
//        SMILESList.add("O=[N+]([O-])c2ccc(Oc1ccc(cc1Cl)C(F)(F)F)cc2(OCC)");
//        SMILESList.add("O=C(c1ccc(cc1[N+](=O)[O-])S(=O)(=O)C)C2C(=O)CCCC2(=O)");
        // for CHRAB
        //SMILESList.add("c1ccc(cc1)CC"); 
        //SMILESList.add("Nc1ccc(cc1)Sc2ccc(N)cc2");
//        SMILESList.add("Oc1cc(c(cc1C(C)(C)C)Sc2cc(c(O)cc2C)C(C)(C)C)C");
        SMILESList.add("N#CSCSC#N");       
        for(String smi : SMILESList){
        
            
            System.out.println(" SMILES to test:"+ smi);
            CoralGraphAttributes coral_attr = new CoralGraphAttributes(smi);

            ArrayList<String> AttributesEC = coral_attr.CalculateEC(3);
            for (String att : AttributesEC)
                System.out.println(att);
            
            ArrayList<String> Attributes = coral_attr.CalculateNNC();
            for (String att : Attributes)
                System.out.println(att);
        
            for (int i=6; i>4;i--){
                String AttributeRings = coral_attr.CalculateRings(i);
                System.out.println(AttributeRings);
            }
            
            CoralSMILES coral_smiles = new CoralSMILES();
            ArrayList<String> list_tokens =  coral_smiles.ProcessSMILES(smi);
            ArrayList<String> attrSkList =  CoralAttributes.calculateAttributeSks(list_tokens);
            for (String att : attrSkList)
                System.out.println(att);
            
            ArrayList<String> attrSSkList =  CoralAttributes.calculateAttributeSSks(list_tokens);
            for (String att : attrSSkList)
                System.out.println(att);
            
            ArrayList<String> attrSSSkList =  CoralAttributes.calculateAttributeSSSks(list_tokens);
            for (String att : attrSSSkList)
                System.out.println(att);
            
            
            String attrHARD =  CoralAttributes.calculateAttributeHARDCoral2017(list_tokens);
            System.out.println(attrHARD);
           
           
        }
    }
    
}
