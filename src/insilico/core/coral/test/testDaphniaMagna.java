package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.biocides.daphniamagna.OptimizedCoralDaphnia;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class testDaphniaMagna {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
               
        CoralModel model = new OptimizedCoralDaphnia();

        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = model.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(line);
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();

    }
}
