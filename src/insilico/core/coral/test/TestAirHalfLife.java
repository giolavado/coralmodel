package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.airhalflife.CoralAirHalfLife;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class TestAirHalfLife {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
        CoralModel model = new CoralAirHalfLife();  
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = model.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();
    }
    
}
