package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.hydrolysis.CoralHydrolysis;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Giovanna Lavado
 */
public class TestCoralHydrolysis {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
 
        CoralModel modelHydrolysis = new CoralHydrolysis();  
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = modelHydrolysis.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();
    }
    
}
