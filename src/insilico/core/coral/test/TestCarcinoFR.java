package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.carcino.CoralFRCarcinogenicity;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class TestCarcinoFR {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
 
        String line;

        // Female rat carcino model
        CoralModel modelCarcino_FR = new CoralFRCarcinogenicity();  
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        

        while ((line = reader.readLine()) != null){
            double pred = modelCarcino_FR.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();



    }
    
}
