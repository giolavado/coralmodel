package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.noael.GAOnoael;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Giovanna Lavado
 */
public class testGAONOAEL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];

        CoralModel model = new GAOnoael();
        
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        String line;

        while ((line = reader.readLine()) != null){
            double pred = model.Predict(line.trim());
            System.out.println(pred);
        }
         reader.close();
        
    }
    
}
