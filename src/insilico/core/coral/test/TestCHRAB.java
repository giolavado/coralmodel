package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.CHRAB.CoralCHRAB;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class TestCHRAB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
 
        CoralModel CHBRModel = new CoralCHRAB();
        String line;
       
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        
        while ((line = reader.readLine()) != null){
            double pred = CHBRModel.Predict(line.trim());
            writer.write(Double.toString(pred));
            writer.write("\r\n");
            System.out.println(pred);
        }
        
        reader.close();
        writer.close();
        
/*        
        //String smi = "O=[N+]([O-])c1ccc(cc1)Cl";
        //String smi = "O=C(OC1CCC25(CC35(CCC4(C)(C(CCC4(C)(C3(CCC2(C1(C)(C)))))C(C)CCC=C(C)C))))C=Cc6ccc(O)c(OC)c6";
        //String smi = "O(c1ccc(N)c(c1)C)C";
        String smi = "O=C1NCCCCC1";
      
        double myclass;
        double pred = CHBRModel.Predict(smi);
       if (pred >0 ) myclass = 1;
       else myclass = -1;
       
       System.out.println(pred);
       System.out.println(myclass);
*/

    }
    
}
