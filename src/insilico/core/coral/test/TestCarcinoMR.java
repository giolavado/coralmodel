package insilico.core.coral.test;

import insilico.core.coral.CoralModel;
import insilico.core.coral.models.carcino.CoralMRCarcinogenicity;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class TestCarcinoMR {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String smi_file = args[0];
        String preds_file = args[1]; 
 
        String line;

        // Male rat carcino model
        CoralModel modelCarcino_MR = new CoralMRCarcinogenicity();
        BufferedReader reader_carcino_MR = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer_carcino_MR = new BufferedWriter(new FileWriter(preds_file));
        
        while ((line = reader_carcino_MR.readLine()) != null){
            double pred = modelCarcino_MR.Predict(line.trim());
            writer_carcino_MR.write(Double.toString(pred));
            writer_carcino_MR.write("\r\n");
            System.out.println(pred);
        }

        reader_carcino_MR.close();
        writer_carcino_MR.close();


    }
    
}
