package insilico.core.coral.test;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralGraphAttributes;
import insilico.core.coral.CoralSMILES;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import org.openscience.cdk.exception.CDKException;

/**
 *
 * @author Giovanna Lavado
 */
public class testGraphAttributes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws GenericFailureException, InvalidMoleculeException, CDKException, Exception {
        
        String smi_file = args[0];
        
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        
        ArrayList<String> SMILESList = new ArrayList<>();
        
        String line;

        while ((line = reader.readLine()) != null){
            
            SMILESList.add(line.trim());
            System.out.println(line);
            
        }
        
        reader.close();
        
        int n = 0;
        
        for(String smi : SMILESList){
        
            n = n+1;        
            
            System.out.println(n + " SMILES to test:"+ smi);
            CoralGraphAttributes coral_attr = new CoralGraphAttributes(smi);

            ArrayList<String> AttributesEC = coral_attr.CalculateEC(1);
        
            for (String att : AttributesEC)
                System.out.println(att);
            
            ArrayList<String> Attributes = coral_attr.CalculateNNC();
        
            for (String att : Attributes)
                System.out.println(att);
        
            for (int i=6; i>4;i--){
                String AttributeRings = coral_attr.CalculateRings(i);
                System.out.println(AttributeRings);
            }
            
            CoralSMILES coral_smiles = new CoralSMILES();
            ArrayList<String> list_tokens =  coral_smiles.ProcessSMILES(smi);
            ArrayList<String> attrSkList =  CoralAttributes.calculateAttributeSks(list_tokens);
            for (String att : attrSkList)
                System.out.println(att);
            
            ArrayList<String> attrSSkList =  CoralAttributes.calculateAttributeSSks(list_tokens);
            for (String att : attrSSkList)
                System.out.println(att);
            
            String attrHARD =  CoralAttributes.calculateAttributeHARDCoral2017(list_tokens);
            System.out.println(attrHARD);
           
           
        }
    }
    
}
