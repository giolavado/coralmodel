package insilico.core.coral.test;

import insilico.core.coral.CoralAttributes;
import insilico.core.coral.CoralSMILES;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import java.util.ArrayList;
import org.openscience.cdk.exception.CDKException;

/**
 *
 * @author Giovanna Lavado
 */
public class testAPPAttr {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws GenericFailureException, InvalidMoleculeException, CDKException, Exception {
           
        ArrayList<String> SMILESList = new ArrayList<>();
        SMILESList.add("O=C(O)C(Oc1ccc(cc1C)Cl)C");    
        SMILESList.add("O=[N+]([O-])c1cc(c(cc1(NCCO))NCCO)Cl");  
        SMILESList.add("O=[N+]([O-])c1cc(ccc1(NC))N(CCO)CCO");
        SMILESList.add("O=C5OC(CC)CCCC(OC1OC(C)C(N(C)C)CC1)C(C(=O)C6CC4C(CCC3CC(OC2CC(C)C(OC)C(OC)C2(OC))CC34)C6(C5))C");
        SMILESList.add("O=C3c1cc(ccc1c2c3(cc(cc2[N+](=O)[O-])[N+](=O)[O-]))[N+](=O)[O-]");
        SMILESList.add("O=C3OC(CC)C(O)(C)C(O)C(C(=O)C(C)CC(O)(C)C(OC1OC(C)CC(N(C)C)C1(O))C(C)C(OC2OC(C)C(O)C(OC)(C)C2)C3C)C");
        SMILESList.add("O=C(O)C(Oc1ccc(cc1C)Cl)C");
        CoralSMILES coral_smiles = new CoralSMILES();
        
        for(String smi : SMILESList){
        
            
            System.out.println(" SMILES to test:"+ smi);
            
            ArrayList<String> list_tokens =  coral_smiles.ProcessSMILES(smi);
            
            ArrayList<String> chemicalList = new ArrayList<>();
            chemicalList.add("N");
            chemicalList.add("O");
            chemicalList.add("S");
            chemicalList.add("P");
            
            ArrayList<String> attrList = 
                    CoralAttributes.calculateAttributesVectorAPP(list_tokens, "Cl", chemicalList);
            
            for (String att : attrList)
                System.out.println(att);
            
            System.out.println("All vector APP");
            
            attrList = 
                    CoralAttributes.calculateAttributesAllVectorAPP(list_tokens);
            
            for (String att : attrList)
                System.out.println(att);
            
                

        }
    }
    
}
